///////////////////////////////////////
//see LICENSE.iitm
///////////////////////////////////////
/*
Authors     : Vinod.G, Aditya Govardhan, Shalender Kumar
Email       : g.vinod1993@gmail.com, cs18m050@smail.iitm.ac.in
See LICENSE for more details
Description:
This module performs the sign injection on the floating point value taken from the rs1 register.
The different instructions work as follows
FSGNJ : Operation bit - 000, The final result is same as that of operand1 but has the sign of operand 2's sign.
FSGNJN :  Operation bit - 001, The final result is same as that of operand 1 but has the opposite sign of operand 2's sign
FSGNJX : Operation bit - 010, The final result is same as that of operand 1 but the sign bit is the exclusive-or of operand 1 and operand 2
*/

package fpu_sign_injection;
import fpu_common ::*;
interface Ifc_fpu_sign_injection#(numeric type e, numeric type m);
//    method Tuple2#(FloatingPoint#(e,m),Exception) sendreceive(FloatingPoint#(e,m) operand1,FloatingPoint#(e,m) operand2, Bit#(3) operation);
    method ReturnType#(e,m) sendreceive(FloatingPoint#(e,m) operand1,FloatingPoint#(e,m) operand2, Bit#(3) operation);
endinterface


module mk_fpu_sign_injection(Ifc_fpu_sign_injection#(e,m));

//    method Tuple2#(FloatingPoint#(e,m),Exception) sendreceive(FloatingPoint#(e,m) operand1,FloatingPoint#(e,m) operand2, Bit#(3) operation);
    method ReturnType#(e,m) sendreceive(FloatingPoint#(e,m) operand1,FloatingPoint#(e,m) operand2, Bit#(3) operation);

        if(operation == 3'b000)                           //FSGNJ
            operand1.sign = operand2.sign;
        else if(operation == 3'b001)			   //FSNGNJN
            operand1.sign = unpack(~pack(operand2.sign));
        else 						   //FSGNJX
            operand1.sign=unpack(pack(operand1.sign)^pack(operand2.sign));
    
        Exception lv_exception = unpack(0);

//        return tuple2(operand1,lv_exception);
//	return ReturnType{valid:1,value:canonicalize(operand1),ex:lv_exception};
	return ReturnType{valid:1,value:operand1,ex:lv_exception};
    endmethod
endmodule
// (*synthesize*)
module mk_fpu_sign_injection_dp_instance(Ifc_fpu_sign_injection#(11,52));
    let ifc();
    mk_fpu_sign_injection _temp(ifc);
    return ifc;
endmodule
// (*synthesize*)
module mk_fpu_sign_injection_sp_instance(Ifc_fpu_sign_injection#(8,23));
    let ifc();
    mk_fpu_sign_injection _temp(ifc);
    return ifc;
endmodule

// module mkTb();
//     Reg#(int) rg_cycle <- mkReg(0);
//     Ifc_fpu_sign_injection#(8,23) ifc <- mk_fpu_sign_injection();
//     rule rl_cycle;
// //	$display("Welcome to IITM %d",`stim_size);
//         rg_cycle <= rg_cycle+1;
//         if(rg_cycle >10)
//             $finish(0);
//     endrule
//     rule rl_stage1(rg_cycle==1);
//         FloatingPoint#(8,23) op1 = FloatingPoint{
//             sign: False,
//             exp: '1,
//             sfd: 10
//         };
//         FloatingPoint#(8,23) op2 = FloatingPoint{
//             sign: True,
//             exp: '1,
//             sfd: 10
//         };
// //        match {.res,.exc} = ifc.sendreceive(op1,op2,3'b000);
//         let x = ifc.sendreceive(op1,op2,3'b000); 
//         $display("op1 sign = %b",x.value.sign);
//     endrule
//     rule rl_stage2(rg_cycle==2);
//         FloatingPoint#(8,23) op1 = FloatingPoint{
//             sign: False,
//             exp: '1,
//             sfd: 10
//         };
//         FloatingPoint#(8,23) op2 = FloatingPoint{
//             sign: True,
//             exp: '1,
//             sfd: 10
//         };
// //        match {.res,.exc} = ifc.sendreceive(op1,op2,3'b001); 
//         let x = ifc.sendreceive(op1,op2,3'b001); 
//         $display("op1 sign = %b",x.value.sign);
//     endrule
//     rule rl_stage3(rg_cycle==3);
//         FloatingPoint#(8,23) op1 = FloatingPoint{
//             sign: False,
//             exp: '1,
//             sfd: 10
//         };
//         FloatingPoint#(8,23) op2 = FloatingPoint{
//             sign: True,
//             exp: '1,
//             sfd: 10
//         };
// //        match {.res,.exc} = ifc.sendreceive(op1,op2,3'b010); 
//         let x = ifc.sendreceive(op1,op2,3'b010); 
//         $display("op1 sign = %b",x.value.sign);
// //        $display("op1 sign = %b",res.sign);
//     endrule
// endmodule 
endpackage
