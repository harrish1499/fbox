//////////////////////////////
// see LICENSE.iitm
//////////////////////////////
/*
--------------------------------------------------------------------------------------------------
Author: Neel Gala
Email id: neelgala@gmail.com
Details:
--------------------------------------------------------------------------------------------------
*/
package Tb_fp_conv; 
 `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;

  import RegFile :: * ;
  import fp_convert_pipelined ::*;


  (*synthesize*)
  module mktb_conv_spdp(Empty);
    RegFile#(Bit#(23) , Bit#(104)) stimulus <- mkRegFileLoad("dp_naz.txt", 0, 4999999);
    let fconv <- mk_fpu_convert_sp_to_dp();

    Reg#(Bit#(23)) read_index <- mkReg(0);
    Reg#(Bit#(64)) ex_out <- mkReg(0);
    Reg#(Bit#(8)) ex_flags <- mkReg(0);
     
    Reg#(Bit#(8)) exp_flags <- mkReg(0);
    Reg#(Bit#(64)) exp_output <- mkReg(0);
    

    rule rl_pick_stimulus_entry;
      
      let _e = stimulus.sub(read_index);
      Bit#(8) e_flags = truncate(_e);
      _e = _e >> 8;
      Bit#(64) e_out = truncate(_e);
      _e = _e >> 64;
      Bit#(32) _inp = truncate(_e);
      
      let op = FloatingPoint{sign : unpack(_inp[31]), exp: _inp[30:23], sfd: _inp[22:0]};
    //  `logLevel( tb, 0, $format("TB: Sending inputs[%d]: op1:%h op2:%h, output %h", read_index, op1, op2,e_out1))
               
      let x <-  fconv.sp_to_dp(tuple2(op, Rnd_Zero));  
      let valid = x.valid;
      let out = x.value;
      let flags = x.ex;
      if(valid==1) begin
        Bit#(64) _out = {pack(out.sign), out.exp, out.sfd};
     
        if( _out != pack(e_out)) begin
         $display("TB: Outputs mismatch[%d]. E:%h R:%h,flag %h",read_index, pack(e_out), _out,flags);
        end
        else begin
          //$display("TB: Outputs match[%d]. E:%h R:%h,flag %h",read_index, pack(exp_output), _out,flags);
        end
     
        if (flags != unpack(truncate(pack(e_flags)))) begin
          $display("TB: Flags mismatch[%d]. E:%h R:%h output G: %h , R: %h ",read_index, pack(e_flags), flags,pack(e_out),_out);
        end
      end

      read_index <= read_index + 1;
      if(read_index == 49)
        $finish(0);
    endrule

  endmodule

  (*synthesize*)
  module mktb_conv_dpsp(Empty);
    RegFile#(Bit#(23) , Bit#(104)) stimulus <- mkRegFileLoad("dp_z.txt", 0, 4999999);
    let fconv <- mk_fpu_convert_dp_to_sp();

    Reg#(Bit#(23)) read_index <- mkReg(0);
    Reg#(Bit#(32)) ex_out <- mkReg(0);
    Reg#(Bit#(8)) ex_flags <- mkReg(0);
     
    Reg#(Bit#(8)) exp_flags <- mkReg(0);
    Reg#(Bit#(32)) exp_output <- mkReg(0);
    Reg#(int) count <- mkReg(0);
    Reg#(Bool) _ready <- mkReg(False);

    rule counter;
      if(count==1)
        _ready<=True;
      else
        count<=count+1;
    endrule

    
    rule rl_pick_stimulus_entry;
      let _e = stimulus.sub(read_index);
      Bit#(8) e_flags = truncate(_e);
      _e = _e >> 8;
      Bit#(32) e_out = truncate(_e);
      _e = _e >> 32;
      Bit#(64) _inp = truncate(_e);
      
      let op = FloatingPoint{sign : unpack(_inp[63]), exp: _inp[62:52], sfd: _inp[51:0]};
      
    //  `logLevel( tb, 0, $format("TB: Sending inputs[%d]: op1:%h op2:%h, output %h", read_index, op1, op2,e_out1))
      exp_output <= e_out;
      exp_flags <= e_flags; 
      fconv.start(tuple2(op, Rnd_Zero));
      
      read_index <= read_index + 1;
      if(read_index == 4999999)
        $finish(0);
    endrule
    
    rule rl_check_output;//(_ready==True);
       
      let x =  fconv.receive();  
      let valid = x.valid;
      let out = x.value;
      let flags = x.ex;
      if(valid==1) begin
        Bit#(32) _out = {pack(out.sign), out.exp, out.sfd};
     
        if( _out != pack(exp_output)) begin
          $display("TB: Outputs mismatch[%d]. E:%h R:%h,flag %h",read_index, pack(exp_output), _out,flags);
        end
        else begin
          //$display("TB: Outputs match[%d]. E:%h R:%h,flag %h",read_index, pack(exp_output), _out,flags);
        end
     
        if (flags != unpack(truncate(pack(exp_flags)))) begin
          $display("TB: Flags mismatch[%d]. E:%h R:%h output G: %h , R: %h ",read_index, pack(exp_flags), flags,pack(exp_output),_out);
        end
      end
    endrule

  endmodule

endpackage

