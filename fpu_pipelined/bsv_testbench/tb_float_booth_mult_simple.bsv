//////////////////////////////
// see LICENSE.iitm
//////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Nitya Ranganathan, Nagakaushik Moturi
Email id: nitya.ranganathan@gmail.com, ee17b111@smail.iitm.ac.in
Details: Simple testbench for a few direct inputs
--------------------------------------------------------------------------------------------------
*/
package tb_float_booth_mult_simple;

  import fpu_booth_multiplier :: * ;
  import fpu_common :: *;
  `include "../modules_fp/bsv_testbench/Logger.bsv"

  (*synthesize*)
  module mktb_float_booth_mult_simple();
    Ifc_fpu_multiplier_sp ifc_mult <- mk_fpu_multiplier_sp();
    Reg#(Bit#(8)) rg_cycle <- mkReg(0);

    rule rl_send0(rg_cycle==0);
      FloatingPoint#(8, 23) f,g;
      RoundMode r1 = Rnd_Nearest_Even;

      f.sign = True;
      f.exp = 8'h00;
      f.sfd = 23'h0008ff;
      
      g.sign = False;
      g.exp = 8'h9e;
      g.sfd = 23'h7ffff3;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

      `logLevel( tb, 1, $format("TB mult: cycle %d: send operands ", rg_cycle, fshow(f)))
    endrule
    
    rule rl_send1(rg_cycle==1);
      FloatingPoint#(8, 23) f,g;
      RoundMode r1 = Rnd_Nearest_Even;

      f.sign = True;
      f.exp = 8'h99;
      f.sfd = 23'h7a1162;
      
      g.sign = False;
      g.exp = 8'h67;
      g.sfd = 23'h01ffdf;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

      `logLevel( tb, 1, $format("TB mult: cycle %d: send operands ", rg_cycle, fshow(f)))
    endrule
    
    rule rl_send2(rg_cycle==2);
      FloatingPoint#(8, 23) f,g;
      RoundMode r1 = Rnd_Nearest_Even;

      f.sign = True;
      f.exp = 8'h82;
      f.sfd = 23'h438060;
      
      g.sign = False;
      g.exp = 8'h7f;
      g.sfd = 23'h7f100d;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

      `logLevel( tb, 1, $format("TB mult: cycle %d: send operands ", rg_cycle, fshow(f)))
    endrule
    
    rule rl_send3(rg_cycle==3);
      FloatingPoint#(8, 23) f,g;
      RoundMode r1 = Rnd_Nearest_Even;

      f.sign = False;
      f.exp = 8'h00;
      f.sfd = 23'h000000;
      
      g.sign = True;
      g.exp = 8'h7f;
      g.sfd = 23'h7f100d;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

      `logLevel( tb, 1, $format("TB mult: cycle %d: send operands ", rg_cycle, fshow(f)))
    endrule

    rule rl_receive;
      ReturnType#(8, 23) r;

      r = ifc_mult.receive();
      
      $display("cycle:",rg_cycle," ",fshow(r));

      `logLevel( tb, 1, $format("TB mult: cycle %d: receive result ", rg_cycle, fshow(r)))
    endrule

    rule rl_end;
      rg_cycle <= rg_cycle + 1;
      if (rg_cycle > 10) begin
        $finish(0);
      end
    endrule

  endmodule
endpackage

